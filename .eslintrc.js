module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    camelcase: ['off', {
      properties: 'never',
    }],
    'no-useless-concat': 'off',
    'no-console': ['error', {
      allow: ['warn', 'error'],
    }],
    'no-alert': 'off',
    'no-unused-vars': 'warn',

  },
};
